package com.telstra.codechallenge.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.telstra.codechallenge.helper.ServiceDataHelper;
import com.telstra.codechallenge.quotes.controller.SpringBootQuotesController;
import com.telstra.codechallenge.quotes.service.SpringBootQuotesService;

@RunWith(MockitoJUnitRunner.class)
public class SpringBootQuotesControllerTest {
	
	@Mock
	SpringBootQuotesService quoteService;
	
	@InjectMocks
	SpringBootQuotesController bootQuotesController;

	@Test
	public void testControllerQuotesMethod() {
		
		when(quoteService.getQuotes()).thenReturn(ServiceDataHelper.getQuotes());
		
		assertThat((bootQuotesController.quotes()).size() == 1);
		assertThat(!(bootQuotesController.quotes()).isEmpty());
		verify(quoteService, atLeast(1)).getQuotes();
	}
	
	@Test
	public void testControllerRandomQuoteMethod() {
		when(quoteService.getRandomQuote()).thenReturn(ServiceDataHelper.getQuote());
		
		assertThat(bootQuotesController.quote() != null);
		assertEquals("success", bootQuotesController.quote().getType());
		verify(quoteService, atLeast(1)).getRandomQuote();
	}
	
}
