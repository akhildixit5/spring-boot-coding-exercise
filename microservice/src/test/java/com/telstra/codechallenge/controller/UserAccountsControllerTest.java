package com.telstra.codechallenge.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.telstra.codechallenge.accounts.controller.UserAccountsController;
import com.telstra.codechallenge.accounts.dto.AccountResponseDto;
import com.telstra.codechallenge.accounts.service.AccountService;
import com.telstra.codechallenge.helper.ServiceDataHelper;

@RunWith(MockitoJUnitRunner.class)
public class UserAccountsControllerTest {
	
	@Mock
	AccountService accountService;
	
	@InjectMocks
	UserAccountsController accountsController;
	
	@Test
	public void testControllerMethod() throws Exception {
		when(accountService.getAccounts(1)).thenReturn(ServiceDataHelper.getUserAccForOne());
		
		assertThat((accountsController.getAccounts(1)).size() == 1);
		assertThat((accountsController.getAccounts(0)).size() == 0);
		
		verify(accountService, atLeast(1)).getAccounts(Mockito.anyInt());
	}
	
	@Test
	public void testControllerMethodReturnedData() throws Exception {
		when(accountService.getAccounts(1)).thenReturn(ServiceDataHelper.getUserAccForOne());
		
		assertThat(accountsController.getAccounts(1).contains(new AccountResponseDto("washwillah", 67219879, "https://github.com/washwillah")));
	}
	

}
