package com.telstra.codechallenge.helper;

import java.util.ArrayList;
import java.util.List;

import com.telstra.codechallenge.accounts.dto.AccountResponseDto;
import com.telstra.codechallenge.accounts.dto.Items;
import com.telstra.codechallenge.accounts.dto.UserAccountDto;
import com.telstra.codechallenge.quotes.dto.Quote;
import com.telstra.codechallenge.quotes.dto.Quote.Value;

public class ServiceDataHelper {	
	
	public static List<AccountResponseDto> getUserAccForOne() {
	
		List<AccountResponseDto> accountDto = new ArrayList<>();
		accountDto.add(new AccountResponseDto("washwillah", 67219879, "https://github.com/washwillah"));
		return accountDto;
	}
	
	
	public static UserAccountDto getUserAccount() {
		
		UserAccountDto userAccount = new UserAccountDto();
		userAccount.setTotal_count(50148533);
		userAccount.setIncomplete_results(false);
		userAccount.setItems(getItems());
		
		return userAccount;
	}
	
	public static List<Items> getItems() {
		List<Items> items = new ArrayList<>();
		Items item = new Items();
		
		item.setLogin("washwillah");
		item.setId(67219879);
		item.setHtml_url("https://github.com/washwillah");
		
		items.add(item);
		return items;
	}
	
	public static Quote[] getQuotes(){
		Quote[] quotes = new Quote[] {getQuote()};
		
		return quotes;
	}
	
	public static Quote getQuote() {
		Quote quote = new Quote();
		Value value = quote.new Value();
		
		value.setId((long) 1);
		value.setQuote("Working with Spring Boot is like pair-programming with the Spring developers.");
		
		quote.setType("success");
		quote.setValue(value);
		
		return quote;
	}

}
