package com.telstra.codechallenge.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import com.telstra.codechallenge.helper.ServiceDataHelper;
import com.telstra.codechallenge.quotes.dto.Quote;
import com.telstra.codechallenge.quotes.service.SpringBootQuotesService;

@RunWith(MockitoJUnitRunner.class)
public class SpringBootQuotesServiceTest {

	@Mock
	RestTemplate restTemplate;
	
	@InjectMocks
	SpringBootQuotesService bootQuotesService;
	
	@Test
	public void testQuotesMethod() {
		when(restTemplate.getForObject(bootQuotesService.getQuotesBaseUrl() + "/api", Quote[].class))
		.thenReturn(ServiceDataHelper.getQuotes());
		
		assertThat(Arrays.asList(bootQuotesService.getQuotes()).contains(ServiceDataHelper.getQuote()));
		assertThat((bootQuotesService.getQuotes()).length == 1);
		verify(restTemplate, atLeast(1)).getForObject(bootQuotesService.getQuotesBaseUrl() + "/api", Quote[].class);
	}
	
	@Test
	public void testRandomQuoteMethod() {
		when(restTemplate.getForObject(bootQuotesService.getQuotesBaseUrl() + "/api/random", Quote.class))
		.thenReturn(ServiceDataHelper.getQuote());
		
		assertThat(null != bootQuotesService.getRandomQuote());
		assertEquals("Working with Spring Boot is like pair-programming with the Spring developers.", bootQuotesService.getRandomQuote().getValue().getQuote());
		verify(restTemplate, atLeast(1)).getForObject(bootQuotesService.getQuotesBaseUrl() + "/api/random", Quote.class);
	}
	
	@Test
	public void testFalbackMethods() throws Exception {
		assertEquals(new Quote(), bootQuotesService.quoteFallback());
	}
}
