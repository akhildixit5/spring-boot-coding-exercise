package com.telstra.codechallenge.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.exceptions.misusing.MissingMethodInvocationException;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import com.telstra.codechallenge.accounts.dto.UserAccountDto;
import com.telstra.codechallenge.accounts.service.AccountServiceImpl;
import com.telstra.codechallenge.helper.ServiceDataHelper;


@RunWith(MockitoJUnitRunner.class)
public class AccountServiceImplTest {

	@Mock
	RestTemplate restTemplate;
	
	@InjectMocks
	AccountServiceImpl accountServiceImpl;
	
	@Test
	public void testGetAccountsForZero() throws Exception {
		assertEquals(Collections.emptyList(), accountServiceImpl.getAccounts(0));
	}
	
	@Test
	public void testGetAccounts() throws Exception {
		when(restTemplate.getForObject(accountServiceImpl.getAccountsBaseUrl(), UserAccountDto.class))
		.thenReturn(ServiceDataHelper.getUserAccount());
		assertEquals(ServiceDataHelper.getUserAccForOne(), accountServiceImpl.getAccounts(1));
		verify(restTemplate, atLeast(1)).getForObject(accountServiceImpl.getAccountsBaseUrl(), UserAccountDto.class);
	}
	
	@Test
	public void testNullValueFromGit() throws Exception {
		when(restTemplate.getForObject(accountServiceImpl.getAccountsBaseUrl(), UserAccountDto.class))
		.thenReturn(new UserAccountDto());
		
		assertEquals(Collections.emptyList(), accountServiceImpl.getAccounts(1));
	}
	
	@Test(expected = MissingMethodInvocationException.class)
	public void testExceptionForFetchingUrl() {
		when(accountServiceImpl.getAccountsBaseUrl()).thenThrow(MissingMethodInvocationException.class);
		accountServiceImpl.getAccountsBaseUrl();
	}
	
	@Test
	public void testFalbackMethodReturnsEmptyList() throws Exception {
		assertEquals(Collections.emptyList(), accountServiceImpl.getAccountsFallback(1));
	}
	
}
