package com.telstra.codechallenge.quotes.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Quote {

  private String type;
  private Value value;

  public String getType() {
	return type;
}

public void setType(String type) {
	this.type = type;
}

public Value getValue() {
	return value;
}

public void setValue(Value value) {
	this.value = value;
}

@JsonIgnoreProperties(ignoreUnknown = true)
  @Data
  public class Value {

    private Long id;
    private String quote;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getQuote() {
		return quote;
	}
	public void setQuote(String quote) {
		this.quote = quote;
	}
  }
}
