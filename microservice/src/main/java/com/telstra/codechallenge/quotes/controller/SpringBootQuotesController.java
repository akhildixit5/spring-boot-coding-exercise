package com.telstra.codechallenge.quotes.controller;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.telstra.codechallenge.quotes.dto.Quote;
import com.telstra.codechallenge.quotes.service.SpringBootQuotesService;

@RestController
public class SpringBootQuotesController {

  private static final Logger logger = LoggerFactory.getLogger(SpringBootQuotesController.class);

  private SpringBootQuotesService springBootQuotesService;
  
  public SpringBootQuotesController(
      SpringBootQuotesService springBootQuotesService) {
	  this.springBootQuotesService = springBootQuotesService;
  }

  @RequestMapping(path = "/quotes", method = RequestMethod.GET)
  public List<Quote> quotes() {
	  logger.info("Inside SpringBootQuotesController, quotes method");
	  
	  return Arrays.asList(springBootQuotesService.getQuotes());
  }

  @RequestMapping(path = "/quotes/random", method = RequestMethod.GET)
  public Quote quote() {
	  logger.info("Inside SpringBootQuotesController, random quote method");
	  
	  return springBootQuotesService.getRandomQuote();
  }
  
}
