package com.telstra.codechallenge.quotes.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.telstra.codechallenge.quotes.dto.Quote;

@Service
public class SpringBootQuotesService {
	
  private static final Logger logger = LoggerFactory.getLogger(SpringBootQuotesService.class);

  @Value("${quotes.base.url}")
  private String quotesBaseUrl;

  public String getQuotesBaseUrl() {
	return quotesBaseUrl;
}

private RestTemplate restTemplate;

  public SpringBootQuotesService(RestTemplate restTemplate) {
	  this.restTemplate = restTemplate;
  }

  /**
   * Returns an array of spring boot quotes. Taken from https://spring.io/guides/gs/consuming-rest/.
   *
   * @return - a quote array
   */
  @HystrixCommand(fallbackMethod="quotesFallback")
  public Quote[] getQuotes() {
	  logger.info("Inside SpringBootQuotesService, getQuotes method");
	  
	  return restTemplate.getForObject(quotesBaseUrl + "/api", Quote[].class);
  }

  /**
   * Returns a random spring boot quote. Taken from https://spring.io/guides/gs/consuming-rest/.
   *
   * @return - a quote
   */
  @HystrixCommand(fallbackMethod="quoteFallback")
  public Quote getRandomQuote() {
	  logger.info("Inside SpringBootQuotesService, getRandomQuote method");
	  
	  return restTemplate.getForObject(quotesBaseUrl + "/api/random", Quote.class);
  }
  
  public Quote[] quotesFallback() {
	  logger.info("Inside SpringBootQuotesService, quotes fallback method");
	  
	  Quote[] quotes = new Quote[] {};
	  return quotes;
  }
  
  public Quote quoteFallback() {
	  logger.info("Inside SpringBootQuotesService, random quote fallback method");
	  
	  return new Quote();
  }
}
