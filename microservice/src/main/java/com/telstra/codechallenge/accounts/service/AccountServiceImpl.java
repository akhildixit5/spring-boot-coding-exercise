package com.telstra.codechallenge.accounts.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.telstra.codechallenge.accounts.dto.AccountResponseDto;
import com.telstra.codechallenge.accounts.dto.Items;
import com.telstra.codechallenge.accounts.dto.UserAccountDto;

@Service
public class AccountServiceImpl implements AccountService{
	private static final Logger logger =  LoggerFactory.getLogger(AccountServiceImpl.class);
	
	@Value("${accounts.base.url}")
	private String accountsBaseUrl;

	public String getAccountsBaseUrl() {
		return accountsBaseUrl;
	}

	private RestTemplate restTemplate;
	
	public AccountServiceImpl(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@HystrixCommand(fallbackMethod="getAccountsFallback")
	public List<AccountResponseDto> getAccounts(int accountLimit) throws Exception{
		logger.info("Initiation of getAccounts method in AccountServiceImpl");
		
		List<AccountResponseDto> responseList = new ArrayList<>();
		
		try {
			List<Items> items = new ArrayList<>();
			UserAccountDto accounts = restTemplate.getForObject(accountsBaseUrl, UserAccountDto.class);
			
			if(null != accounts && null != accounts.getItems()) {
				items = accounts.getItems();
				
				logger.info("Data returned from Git API: " + accounts.getTotal_count() + ", " + accounts.isIncomplete_results()
				+ ", " + accounts.getItems());
			}
			
			responseList = items.stream()
					.map(item -> new AccountResponseDto(item.getLogin(), item.getId(), item.getHtml_url()))
					.limit(accountLimit)
					.collect(Collectors.toList());
			
		}catch(Exception ex) {
			logger.error("Error occured in AccountServiceImpl: " + ex);
			throw new Exception();
		}
		
		return responseList;
	}
	
	
	public List<AccountResponseDto> getAccountsFallback(int accountLimit) throws Exception{
		logger.info("Inside AccountServiceImpl fallback method");
		
		List<AccountResponseDto> fallbackList = new ArrayList<>();
		return fallbackList;
	}
	
}
