package com.telstra.codechallenge.accounts.service;

import java.util.List;
import com.telstra.codechallenge.accounts.dto.AccountResponseDto;

@FunctionalInterface
public interface AccountService {

	List<AccountResponseDto> getAccounts(int accountLimit) throws Exception;
}
