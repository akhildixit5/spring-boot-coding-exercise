package com.telstra.codechallenge.accounts.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.telstra.codechallenge.accounts.dto.AccountResponseDto;
import com.telstra.codechallenge.accounts.service.AccountService;

@RestController
public class UserAccountsController {
	private static final Logger logger = LoggerFactory.getLogger(UserAccountsController.class);
	
	@Autowired
	private AccountService accountService;
	
	public UserAccountsController(AccountService accountService) {
		this.accountService = accountService;
	}
	
	@GetMapping("/api/accounts/{accountLimit}")
	public List<AccountResponseDto> getAccounts(@PathVariable int accountLimit) throws Exception{
		logger.info("Inside UserAccountsController, number of accounts requested: "+ accountLimit);
		
		List<AccountResponseDto> responseList = new ArrayList<>();
		try {
			responseList = accountService.getAccounts(accountLimit);
			
		}catch (Exception ex) {
			logger.info("Exception in UserAccountsController: " + ex);
		}
		return responseList;
	}
	
}
