package com.telstra.codechallenge.accounts.dto;

import lombok.Data;

@Data
public class AccountResponseDto {

	public AccountResponseDto(String login, long id, String html_url) {
		super();
		this.login = login;
		this.id = id;
		this.html_url = html_url;
	}
	
	private String login;
    private long id;
    private String html_url;
    
}
